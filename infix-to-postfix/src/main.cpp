#include "InfixToPostfix.hpp"

#include <iostream>
#include <limits>

int main() {
	int menuChoice;

menuStart:
	std::cout << "\n(1) Convert Infix to Postfix\n(2) Evaluate Expression\n(3) Exit" << std::endl;
menuChoice:
	std::cin >> menuChoice;
	if (!std::cin.good()) {
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cerr << "Invalid input." << std::endl;
		goto menuChoice;
	}

	if (menuChoice == 1) {
		InfixToPostfix expr;
		expr.getInfixExpression();
		expr.convertToPostFix();
		expr.print();
	} else if (menuChoice == 2) {
		InfixToPostfix expr;
		expr.getInfixExpression();
		expr.convertToPostFix();
		expr.evaluate();
	} else if (menuChoice == 3) {
		return 0;
	} else {
		std::cerr << "Invalid input." << std::endl;
		goto menuChoice;
	}

	goto menuStart;
}
