

#include "InfixToPostfix.hpp"

#include <functional>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

void InfixToPostfix::getInfixExpression() {
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::cout << "Enter an infix expression. End your expression with \'#\'" << std::endl;
	std::getline(std::cin, ifx);
}

void InfixToPostfix::convertToPostFix() {
	std::function<int(const char)> precedence = [](const char c) {
		if (c == '+' || c == '-') {
			return 1;
		} else if (c == '*' || c == '/') {
			return 2;
		} else {
			return 3;
		}
	};

	std::function<bool(const char, const char)> cmpPrecedence = [&precedence](const char a, const char b) { return precedence(a) >= precedence(b); };

	for (char sym : ifx) {
		if (std::isspace(sym)) {
			continue;
		} else if (std::isalpha(sym)) {
			pfx.emplace_back(sym);
		} else if (sym == '(') {
			stack.push(sym);
		} else if (sym == ')') {
			while (!stack.empty() && stack.top() != '(') {
				pfx.emplace_back(stack.top());
				stack.pop();
			}
			stack.pop();
		} else if (sym == '#') {
			break;
		} else {
			if (stack.empty()) {
				stack.push(sym);
			} else {
				while (!stack.empty() && (stack.top() != '(') && cmpPrecedence(stack.top(), sym)) {
					pfx.emplace_back(stack.top());
					stack.pop();
				}
				stack.push(sym);
			}
		}
	}

	while (!stack.empty()) {
		pfx.emplace_back(stack.top());
		stack.pop();
	}
}

void InfixToPostfix::print() {
	for (auto& ele : pfx) {
		std::cout << ele << ' ';
	}
}

void InfixToPostfix::evaluate() {
	Stack<int> stack;
	int vars[256];
	bool subbed[256];

	for (unsigned int i = 0; i < 256; i++) {
		subbed[i] = false;
	}

	for (auto& ele : pfx) {
		if (std::isalpha(ele)) {
			if (!subbed[ele]) {
				int num;
			start:
				std::cout << ele << " = ";
				std::cin >> num;
				if (!std::cin.good()) {
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					std::cerr << "Invalid input." << std::endl;
					goto start;
				}

				subbed[ele] = true;
				vars[ele] = num;
			}

			stack.push(vars[ele]);
		} else {
			int x = stack.top();
			stack.pop();
			int y = stack.top();
			stack.pop();
			if (ele == '+') {
				stack.push(y + x);
			} else if (ele == '-') {
				stack.push(y - x);
			} else if (ele == '*') {
				stack.push(y * x);
			} else if (ele == '/') {
				stack.push(y / x);
			}
		}
	}

	std::cout << stack.top() << std::endl;
}
