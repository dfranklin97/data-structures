#include <list>
#include <iosfwd>
#include <stack>
#include <string>

template <typename T>
using List = std::list<T>;

template <typename T>
using Stack = std::stack<T>;

struct InfixToPostfix {
	std::string ifx;
	List<char> pfx;
	Stack<char> stack;

	void getInfixExpression();
	void convertToPostFix();
	void print();
	void evaluate();
};
