// Dusty Franklin
// 316 P3 - Topological Sort
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#include "316p3.hpp"

#include <exception>
#include <iostream>
#include <limits>

using Label = int;

int main() {
	int menuChoice;
	std::string filename;
	Graph<Label> graph;

menuStart:
	std::cout << "\n(1) Open input file.\n(2) Topological sort.\n(3) Detect "
				 "Cycle.\n(4) Quit."
			  << std::endl;
menuChoice:
	std::cin >> menuChoice;
	if (!std::cin.good()) {
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cerr << "Invalid input." << std::endl;
		goto menuChoice;
	}

	if (menuChoice == 1) {
		std::cout << "Enter file name: ";
	fileChoice:
		std::cin >> filename;
		if (!std::cin.good()) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cerr << "Invalid input." << std::endl;
			goto fileChoice;
		}

		graph.clear();
		try {
			load(graph, filename);
		} catch (std::exception& ex) {
			std::cerr << "Input must be a DAG." << std::endl;
		}
	} else if (menuChoice == 2) {
		try {
			print(topoSort(graph));
		} catch (std::exception& ex) {
			std::cerr << ex.what() << std::endl;
		}
	} else if (menuChoice == 3) {
		const std::forward_list<const Label*> list = detectCycle(graph);
		if (list.empty()) {
			std::cout << "No Cycle." << std::endl;
		} else {
			std::cout << "Cycle: ";
			print(list);
		}
	} else if (menuChoice == 4) {
		return 0;
	} else {
		std::cerr << "Invalid input." << std::endl;
		goto menuChoice;
	}

	goto menuStart;
}
