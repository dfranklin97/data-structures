// Dusty Franklin
// 316 P3 - Topological Sort
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#include <exception>
#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>

template <typename T>
void load(Graph<T>& graph, const std::string filename) {
	std::string line;
	std::ifstream file(filename.c_str());
	if (file.is_open()) {
		T from;
		T to;
		double weight;

		while (std::getline(file, line)) {
			std::stringstream ss(line);
			ss >> from >> to >> weight;

			if (ss.fail()) {
				throw std::runtime_error("Bad input file.");
			} else {
				graph.insert(from, to, weight);
			}
		}
	} else {
		std::cerr << "Unable to open file." << std::endl;
	}
}

template <typename T>
void print(const std::forward_list<const T*>& list) {
	for (auto& ele : list) {
		std::cout << *ele << ' ';
	}

	std::cout << std::endl;
}

template <typename T>
std::forward_list<const T*> topoSort(const Graph<T>& graph) {
	std::vector<bool> visited(graph.numOfVertexes(), false);
	std::forward_list<const T*> L;

	std::function<void(const T&)> visit = [&visit, &graph, &visited,
										   &L](const T& vertex) {
		int index = graph.findVertexIndex(vertex);

		if (index == -1) {
			std::domain_error("Invalid vertex.");
		} else {
			if (visited[static_cast<unsigned int>(index)] == false) {
				if (graph.numOfChildren(vertex) > 0) {
					for (auto& child : graph.closestChildren(vertex)) {
						visit(*child);
					}
				}

				visited[index] = true;
				L.emplace_front(&vertex);
			}
		}
	};

	std::forward_list<const T*> zeroInDegree = graph.getZeroIndegree();
	if (zeroInDegree.empty()) {
		throw std::domain_error("No zero indegree vertex. Not a DAG.");
	} else {
		for (auto& vertex : zeroInDegree) {
			visit(*vertex);
		}
	}

	return L;
}

template <typename T>
std::forward_list<const T*> detectCycle(const Graph<T>& graph) {
	std::vector<bool> visited(graph.numOfVertexes(), false);
	std::vector<unsigned int> visitedStack;
	std::forward_list<const T*> L;

	visitedStack.reserve(graph.numOfVertexes());

	std::function<void(const unsigned int)> visit =
		[&visit, &graph, &visited, &L,
		 &visitedStack](const unsigned int index) {
			for (auto& ele : visitedStack) {
				if (ele == index) {
					visitedStack.emplace_back(index);
					throw std::exception();
				}
			}
			visitedStack.emplace_back(index);

			for (auto& vertex : graph.closestChildren(graph[index])) {
				for (unsigned int i = 0; i < graph.numOfVertexes(); i++) {
					if (graph[i] == *vertex) {
						visit(i);
						break;
					}
				}
			}

			visitedStack.pop_back();
		};

	for (unsigned int i = 0; i < graph.numOfVertexes(); i++) {
		try {
			visitedStack.clear();
			visit(i);
		} catch (std::exception& ex) {
			std::forward_list<const T*> cycle;

			for (auto it = visitedStack.rbegin(); it != visitedStack.rend();
				 ++it) {
				cycle.emplace_front(&graph[*it]);
			}

			return cycle;
		}
	}

	return std::forward_list<const T*>();
}
