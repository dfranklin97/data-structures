// Dusty Franklin
// 316 P3 - Topological Sort
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#include <cassert>
#include <iostream>
#include <stdexcept>

template <typename T>
void Graph<T>::insert(const T& from, const T& to, const double weight) {
	if (!vertexExists(from)) {
		addVertex(from);
	}

	if (!vertexExists(to)) {
		addVertex(to);
	}

	if (!edgeExists(from, to, weight)) {
		addEdge(from, to, weight);
	} else {
		throw std::runtime_error(
			"Edge with weight " + std::to_string(weight) +
			" can not be added because it is a duplicate.");
	}
}

template <typename T>
void Graph<T>::clear() {
	vertexes.clear();
	edges.clear();
	degree.clear();
}

template <typename T>
bool Graph<T>::vertexExists(const T& val) const {
	for (auto vertex : vertexes) {
		if (vertex == val) {
			return true;
		}
	}

	return false;
}

template <typename T>
bool Graph<T>::edgeExists(const T& from, const T& to) const {
	const unsigned int indexOfFrom = findVertexIndex(from);
	const unsigned int indexOfTo = findVertexIndex(to);

	for (auto& edge : edges) {
		if (edge.indexOfVertexSent == indexOfFrom) {
			for (auto& ele : edge.adjencyList) {
				if (ele.indexOfVertexReceived = indexOfTo) {
					return true;
				}
			}
		}
	}

	return false;
}

template <typename T>
bool Graph<T>::edgeExists(const T& from,
						  const T& to,
						  const double weight) const {
	const unsigned int indexOfFrom = findVertexIndex(from);
	const unsigned int indexOfTo = findVertexIndex(to);

	for (auto& edge : edges) {
		if (edge.indexOfVertexSent == indexOfFrom) {
			for (auto& ele : edge.adjencyList) {
				if (ele.indexOfVertexReceived == indexOfTo &&
					ele.weight == weight) {
					return true;
				}
			}
		}
	}

	return false;
}

template <typename T>
int Graph<T>::findVertexIndex(const T& val) const {
	for (unsigned i = 0; i < vertexes.size(); i++) {
		if (vertexes[i] == val) {
			return i;
		}
	}

	return -1;
}

template <typename T>
T& Graph<T>::operator[](unsigned int index) {
	return vertexes[index];
}

template <typename T>
const T& Graph<T>::operator[](unsigned int index) const {
	return vertexes[index];
}

template <typename T>
unsigned int Graph<T>::numOfVertexes() const {
	return vertexes.size();
}

template <typename T>
int Graph<T>::numOfChildren(const T& val) const {
	int index = findVertexIndex(val);

	if (index == -1) {
		throw std::domain_error("Vertex does not exist.");
	}

	return degree[index].out;
}

template <typename T>
std::forward_list<const T*> Graph<T>::closestChildren(const T& val) const {
	std::forward_list<const T*> L;

	int indexOfFrom = findVertexIndex(val);

	if (indexOfFrom == -1) {
		throw std::domain_error("Vertex does not exist.");
	}

	for (auto ele : edges) {
		if (ele.indexOfVertexSent == static_cast<unsigned int>(indexOfFrom)) {
			std::forward_list<receivedEdge> sortedAdjencyList(ele.adjencyList);

			sortedAdjencyList.sort(
				[](const receivedEdge& a, const receivedEdge& b) {
					return a.weight < b.weight;
				});

			for (auto& ele : sortedAdjencyList) {
				L.emplace_front(&vertexes[ele.indexOfVertexReceived]);
			}

			return L;
		}
	}

	return L;
}

template <typename T>
std::forward_list<const T*> Graph<T>::getZeroIndegree() const {
	std::forward_list<const T*> L;

	for (unsigned int i = 0; i < vertexes.size(); i++) {
		if (degree[i].in == 0) {
			L.emplace_front(&vertexes[i]);
		}
	}

	return L;
}

template <typename T>
void Graph<T>::addVertex(const T& val) {
	vertexes.emplace_back(val);
	edges.emplace_front(vertexes.size() - 1);
	degree.emplace_back();

	assert(vertexes.size() == degree.size());
}

template <typename T>
void Graph<T>::addEdge(const T& from, const T& to, const double weight) {
	const unsigned int indexOfFrom = findVertexIndex(from);
	const unsigned int indexOfTo = findVertexIndex(to);

	for (auto& edge : edges) {
		if (edge.indexOfVertexSent == indexOfFrom) {
			edge.adjencyList.emplace_front(indexOfTo, weight);

			++degree[indexOfFrom].out;
			++degree[indexOfTo].in;

			break;
		}
	}
}
