// Dusty Franklin
// 316 P3 - Topological Sort
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#ifndef graph_guard
#define graph_guard

#include <forward_list>
#include <utility>
#include <vector>

struct receivedEdge {
	receivedEdge(const unsigned int index, const double weight)
		: indexOfVertexReceived(index), weight(weight) {}

	unsigned int indexOfVertexReceived;
	double weight;
};

struct childrenOfVertex {
	childrenOfVertex(const unsigned int index) : indexOfVertexSent(index) {}

	unsigned int indexOfVertexSent;
	std::forward_list<receivedEdge> adjencyList;
};

struct Degree {
	Degree(const unsigned int in = 0, const unsigned int out = 0)
		: in(in), out(out){};

	unsigned int in;
	unsigned int out;
};

template <typename T>
class Graph {
   public:
	void insert(const T& from, const T& to, const double weight);
	void clear();

	bool vertexExists(const T& val) const;
	bool edgeExists(const T& from, const T& to) const;
	bool edgeExists(const T& from, const T& to, const double weight) const;

	int findVertexIndex(const T& val) const;

	T& operator[](unsigned int index);
	const T& operator[](unsigned int index) const;

	unsigned int numOfVertexes() const;
	int numOfChildren(const T& val) const;

	std::forward_list<const T*> closestChildren(const T& val) const;
	std::forward_list<const T*> getZeroIndegree() const;

   private:
	std::vector<T> vertexes;
	std::forward_list<childrenOfVertex> edges;
	std::vector<Degree> degree;

	void addVertex(const T& value);
	void addEdge(const T& from, const T& to, const double weight);
};

#include "Graph.tpp"

#endif
