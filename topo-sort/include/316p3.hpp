// Dusty Franklin
// 316 P3 - Topological Sort
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#ifndef p3_guard
#define p3_guard

#include "Graph.hpp"

template <typename T>
void load(Graph<T>& graph, const std::string filename);

template <typename T>
void print(const std::forward_list<const T*>& list);

template <typename T>
std::forward_list<const T*> topoSort(const Graph<T>& graph);

template <typename T>
std::forward_list<const T*> detectCycle(const Graph<T>& graph);

#include "316p3.tpp"

#endif
