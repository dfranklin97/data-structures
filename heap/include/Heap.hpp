#ifndef Heap_guard
#define Heap_guard

#include <functional>
#include <vector>

template <typename T>
class Heap {
   public:
	bool empty() const;
	unsigned int size() const;

	const T& operator[](const unsigned int index) const;
	const T& top() const;
	std::vector<T> getOrderedList() const;

	void insert(const T& val);

	void pop();
	void del(const unsigned int index);
	void clear();

	void setMode(const std::function<bool(const T&, const T&)>& cmpFn);

   private:
	std::vector<T> data;
	std::function<bool(const T&, const T&)> cmp;

	void heapify(const unsigned int index);
};

#include "Heap.tpp"

#endif
