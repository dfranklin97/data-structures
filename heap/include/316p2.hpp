// Dusty Franklin
// 316 P2 - Heap
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#ifndef p2_guard
#define p2_guard

#include "Heap.hpp"

#include <fstream>
#include <iosfwd>

struct Record {
	char first[21];
	char last[21];
	int ID;
	float GPA;

	void write(std::ofstream& of);
	void read(std::ifstream& inf);
};

std::ostream& operator<<(std::ostream& os, const Record& rec);

std::function<bool(const Record&, const Record&)> cmpRecID = [](const Record& a, const Record& b) { return a.ID > b.ID; };
std::function<bool(const Record&, const Record&)> cmpRecLast = [](const Record& a, const Record& b) { return a.last > b.last; };

template <typename T>
void read(Heap<T>& heap, const std::string filename);

template <typename T>
void print(const Heap<T>& heap);

template <typename T>
void save(const Heap<T>& heap, const std::string filename);

template <typename T>
void import(Heap<T>& heap, const std::string filename);

#include "316p2.tpp"

#endif
