// Dusty Franklin
// 316 P2 - Heap
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#include <exception>
#include <iostream>
#include <stack>
#include <sstream>

void Record::write(std::ofstream& of) {
	of.write(first, sizeof(first));
	of.write(last, sizeof(last));
	of.write(reinterpret_cast<char*>(&ID), sizeof(ID));
	of.write(reinterpret_cast<char*>(&GPA), sizeof(GPA));
}

void Record::read(std::ifstream& inf) {
	inf.read(first, sizeof(first));
	inf.read(last, sizeof(last));
	inf.read(reinterpret_cast<char*>(&ID), sizeof(ID));
	inf.read(reinterpret_cast<char*>(&GPA), sizeof(GPA));
}

std::ostream& operator<<(std::ostream& os, const Record& rec) {
	return os << rec.first << ' ' << rec.last << ' ' << rec.ID << ' ' << rec.GPA;
}

std::istream& operator>>(std::istream& is, Record& rec) {
	return is >> rec.first >> rec.last >> rec.ID >> rec.GPA;
}

template <typename T>
void read(Heap<T>& heap, const std::string filename) {
	std::ifstream file(filename.c_str());
	std::string line;
	Record rec;

	if (file.is_open()) {
		while (std::getline(file, line)) {
			if (!line.empty()) {
				std::stringstream ss(line);
				ss >> rec;
				if (!ss.fail()) {
					heap.insert(rec);
				}
			}
		}
	} else {
		throw std::runtime_error("Unable to open file.");
	}
}

template <typename T>
void print(const Heap<T>& heap) {
	std::stack<T> stack;
	for (const Record& record : heap.getOrderedList()) {
		stack.push(record);
	}

	while (!stack.empty()) {
		std::cout << stack.top() << '\n';
		stack.pop();
	}
	std::cout << std::endl;
}

template <typename T>
void save(const Heap<T>& heap, const std::string filename) {
	std::ofstream file(filename.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
	if (file.is_open()) {
		for (Record& rec : heap.getOrderedList()) {
			rec.write(file);
		}

		file.close();
	} else {
		throw std::runtime_error("Unable to open file.");
	}
}

template <typename T>
void import(Heap<T>& heap, const std::string filename) {
	Record rec;
	std::streampos size;

	std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
	if (file.is_open()) {
		size = file.tellg();
		file.seekg(0, std::ios::beg);

		if (size > 0) {
			while (file.good()) {
				rec.read(file);
				if (!file.fail()) {
					std::cout << rec << '\n';
					heap.insert(rec);
				}
			}
		}

		if (file.fail() && !file.eof()) {
			heap.clear();
			file.clear();
			file.close();
			throw std::runtime_error("Bad input file.");
		}

		file.close();
	} else {
		throw std::runtime_error("Unable to open file.");
	}
}
