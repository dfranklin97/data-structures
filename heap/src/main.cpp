// Dusty Franklin
// 316 P2 - Heap
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#include "316p2.hpp"

#include <exception>
#include <iostream>
#include <limits>

std::string BIN_FILE = "316p2.rec";

int main() {
	bool heapBuilt = false;
	int menuChoice;
	std::string filename;
	Heap<Record> heap;

menuStart:
	try {
		std::cout << "\n(1) Read data from external text file.\n(2) Build a max heap in terms of ID or LastName.\n(3) Add a new record to max heap.\n(4) Delete a record from the max heap.\n(5) Print "
					 "sorted list from max heap in ascending order based on ID or LastName.\n(6) Save a sorted list to external binary file named as 316p2.rec.\n(7) Load the binary file and print "
					 "the list by one record per line.\n(8) Quit."
				  << std::endl;
	menuChoice:
		std::cin >> menuChoice;
		if (!std::cin.good()) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cerr << "Invalid input." << std::endl;
			goto menuChoice;
		}

		if (!heapBuilt && !(menuChoice == 2 || menuChoice == 8)) {
			std::cout << "Heap needs built." << std::endl;
			goto menuChoice;
		}

		if (heap.empty() && (menuChoice == 4 || menuChoice == 5 || menuChoice == 6)) {
			std::cout << "Heap can not be empty." << std::endl;
			goto menuChoice;
		}

		if (menuChoice == 1) {
			int choice;
			filename = "cs316p2.dat";
			std::cout << "\nLoad: " << filename << "\n(0) No.\n(1) Yes." << std::endl;
		defaultFileChoice:
			std::cin >> choice;
			if (!std::cin.good()) {
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				std::cerr << "Invalid input." << std::endl;
				goto defaultFileChoice;
			}

			if (choice == 0) {
				std::cout << "\nEnter file name: ";
			fileChoice:
				std::cin >> filename;
				if (!std::cin.good()) {
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					std::cerr << "Invalid input." << std::endl;
					goto fileChoice;
				}
			} else if (choice == 1) {
				// Do nothing
			} else {
				goto defaultFileChoice;
			}

			read(heap, filename);
		} else if (menuChoice == 2) {
			int type;
			std::cout << "\n(1) ID.\n(2) LastName.\n";
		typeChoice:
			std::cin >> type;
			if (!std::cin.good()) {
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				std::cerr << "Invalid input." << std::endl;
				goto typeChoice;
			}

			if (type == 1) {
				heap.setMode(cmpRecID);
			} else if (type == 2) {
				heap.setMode(cmpRecLast);
			} else {
				std::cerr << "Invalid input." << std::endl;
				goto typeChoice;
			}

			heapBuilt = true;
		} else if (menuChoice == 3) {
			Record record;
			std::cout << "\nEnter Record:\n";
		recordChoice:
			std::cin >> record;
			if (!std::cin.good()) {
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				std::cerr << "Invalid input." << std::endl;
				goto recordChoice;
			}

			heap.insert(record);
		} else if (menuChoice == 4) {
			int del;
			std::cout << "\nDelete by:\n(1) ID.\n(2) LastName.\n";
		delChoice:
			std::cin >> del;
			if (!std::cin.good()) {
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				std::cerr << "Invalid input." << std::endl;
				goto delChoice;
			}

			if (del == 1) {
				int id;
				std::cout << "\nEnter id: ";
			idChoice:
				std::cin >> id;
				if (!std::cin.good()) {
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					std::cerr << "Invalid input." << std::endl;
					goto idChoice;
				}

				for (unsigned int i = 0; i < heap.size(); i++) {
					if (heap[i].ID == id) {
						heap.del(i);
						break;
					}
				}
			} else if (del == 2) {
				std::string last;
				std::cout << "\nEnter last name: ";
			lastChoice:
				std::cin >> last;
				if (!std::cin.good()) {
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					std::cerr << "Invalid input." << std::endl;
					goto lastChoice;
				}

				for (unsigned int i = 0; i < heap.size(); i++) {
					if (heap[i].last == last.data()) {
						heap.del(i);
						break;
					}
				}
			} else {
				std::cerr << "Invalid input." << std::endl;
				goto delChoice;
			}
		} else if (menuChoice == 5) {
			std::cout << '\n';
			print(heap);
		} else if (menuChoice == 6) {
			save(heap, BIN_FILE);
		} else if (menuChoice == 7) {
			heap.clear();
			import(heap, BIN_FILE);
		} else if (menuChoice == 8) {
			return 0;
		} else {
			std::cerr << "Invalid input." << std::endl;
			goto menuChoice;
		}
	} catch (std::exception& ex) {
		std::cerr << ex.what() << std::endl;
	}

	goto menuStart;
}
