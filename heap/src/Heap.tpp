template <typename T>
bool Heap<T>::empty() const {
	return data.empty();
}

template <typename T>
unsigned int Heap<T>::size() const {
	return data.size();
}

template <typename T>
const T& Heap<T>::operator[](const unsigned int index) const {
	return data[index];
}

template <typename T>
const T& Heap<T>::top() const {
	return data[0];
}

template <typename T>
std::vector<T> Heap<T>::getOrderedList() const {
	std::vector<T> list;
	Heap<T> temp(*this);

	list.reserve(size());

	while (!temp.empty()) {
		list.emplace_back(temp.top());
		temp.pop();
	}

	return list;
}

template <typename T>
void Heap<T>::insert(const T& val) {
	unsigned int index = size();
	data.emplace_back(val);
	while (cmp(data[index], data[index / 2])) {
		std::swap(data[index], data[index / 2]);
		index /= 2;
	}
}

template <typename T>
void Heap<T>::pop() {
	del(0);
}

template <typename T>
void Heap<T>::del(const unsigned int index) {
	if (empty()) {
		return;
	} else if (size() == 1) {
		data.clear();
	} else {
		std::swap(data[index], data[size() - 1]);
		data.pop_back();
		heapify(index);
	}
}

template <typename T>
void Heap<T>::clear() {
	data.clear();
}

template <typename T>
void Heap<T>::setMode(const std::function<bool(const T&, const T&)>& cmpFn) {
	cmp = cmpFn;
	clear();
}

template <typename T>
void Heap<T>::heapify(const unsigned int index) {
	unsigned int left = 2 * index;
	unsigned int right = 2 * index + 1;
	unsigned int largest = index;

	if (left < size() && cmp(data[left], data[largest])) {
		largest = left;
	}

	if (right < size() && cmp(data[right], data[largest])) {
		largest = right;
	}

	if (largest != index) {
		std::swap(data[index], data[largest]);
		heapify(largest);
	}
}
