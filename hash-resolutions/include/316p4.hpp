// Dusty Franklin
// 316 P4 - Hash resolutions
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#ifndef p4_guard
#define p4_guard

#include <stdexcept>
#include <utility>
#include <vector>

struct Statistics {
	Statistics();

	int collisionsLinearProbing;
	int collisionsQuadraticProbing;
	int collisionsDoubleHashing;

	int failuresLinearProbing;
	int failuresQuadraticProbing;
	int failuresDoubleHashing;
};

using IntList = std::vector<int>;
using SequenceStatisticsPair = std::pair<IntList, Statistics>;
using ListsOfInts = std::vector<SequenceStatisticsPair>;

int getRandomInt();
IntList makeRandomList(int tableSize);
void generateRandomLists(ListsOfInts& lists, const int numberOfLists);

void linearProbe(SequenceStatisticsPair& randomList);
void quadraticProbe(SequenceStatisticsPair& randomList);
void doubleHash(SequenceStatisticsPair& randomList);

void print(const ListsOfInts& lists);

#endif
