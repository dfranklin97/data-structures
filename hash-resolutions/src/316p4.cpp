// Dusty Franklin
// 316 P4 - Hash resolutions
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#include "316p4.hpp"

#include <chrono>
#include <iostream>
#include <limits>
#include <random>

const int HASH_TABLE_SIZE = 10001;

Statistics::Statistics()
	: collisionsLinearProbing(0),
	  collisionsQuadraticProbing(0),
	  collisionsDoubleHashing(0),
	  failuresLinearProbing(0),
	  failuresQuadraticProbing(0),
	  failuresDoubleHashing(0) {}

namespace {
std::mt19937 rng(std::chrono::system_clock::now().time_since_epoch().count());
std::uniform_int_distribution<int> uni(100000, std::numeric_limits<int>::max());
}  // namespace

int getRandomInt() {
	return uni(rng);
}

IntList makeRandomList(int tableSize) {
	int randomInts[tableSize];
	IntList thisList;

	for (int j = 0; j < tableSize; j++) {
		randomInts[j] = getRandomInt();
		thisList.push_back(randomInts[j]);
	}

	return thisList;
}

void generateRandomLists(ListsOfInts& lists, const int numberOfLists) {
	static bool hasRun = false;

	if (!hasRun) {
		for (int i = 1; i <= numberOfLists; i++) {
			lists.push_back(
				std::make_pair(makeRandomList(1000 * i), Statistics()));
		}

		hasRun = true;
	} else {
		for (int i = 0; i < numberOfLists; i++) {
			lists[i].first = makeRandomList(1000 * (i + 1));
			lists[i].second.collisionsLinearProbing = 0;
			lists[i].second.collisionsQuadraticProbing = 0;
			lists[i].second.collisionsDoubleHashing = 0;
		}
	}
}

void linearProbe(SequenceStatisticsPair& randomList) {
	int j;

	bool empty[HASH_TABLE_SIZE]{true};
	for (size_t i = 0; i < HASH_TABLE_SIZE; i++) {
		empty[i] = true;
	}

	for (auto& num : randomList.first) {
		j = 0;
		int index = num % HASH_TABLE_SIZE;

		while (!empty[index]) {
			++j;

			if (j == HASH_TABLE_SIZE) {
				++randomList.second.failuresLinearProbing;

				throw std::runtime_error("Table full");
			}

			index = (num + j) % HASH_TABLE_SIZE;
		}

		empty[index] = false;
	}

	randomList.second.collisionsLinearProbing = j;
}

void quadraticProbe(SequenceStatisticsPair& randomList) {
	int j;

	bool empty[HASH_TABLE_SIZE]{true};
	for (size_t i = 0; i < HASH_TABLE_SIZE; i++) {
		empty[i] = true;
	}

	for (auto& num : randomList.first) {
		j = 0;
		int index = num % HASH_TABLE_SIZE;

		while (!empty[index]) {
			++j;

			if (j == HASH_TABLE_SIZE) {
				++randomList.second.failuresQuadraticProbing;

				throw std::runtime_error("Table full");
			}

			index = (num + j * j) % HASH_TABLE_SIZE;
		}

		empty[index] = false;
	}

	randomList.second.collisionsQuadraticProbing = j;
}

int doubleHashHelper(int num, int j) {
	return (num % HASH_TABLE_SIZE) + (j * ((num % 17) + 43));
}

void doubleHash(SequenceStatisticsPair& randomList) {
	int j;

	bool empty[HASH_TABLE_SIZE];
	for (size_t i = 0; i < HASH_TABLE_SIZE; i++) {
		empty[i] = true;
	}

	for (auto& num : randomList.first) {
		j = 0;
		int index = doubleHashHelper(num, j) % HASH_TABLE_SIZE;

		while (!empty[index]) {
			++j;

			if (j == HASH_TABLE_SIZE) {
				++randomList.second.failuresDoubleHashing;

				throw std::runtime_error("Table full");
			}

			index = doubleHashHelper(num, j) % HASH_TABLE_SIZE;
		}

		empty[index] = false;
	}

	randomList.second.collisionsDoubleHashing = j;
}

void print(const ListsOfInts& lists) {
	std::cout << "\nSequence Size:\t\t\t\t";
	for (auto& list : lists) {
		std::cout << list.first.size() << '\t';
	}

	std::cout << "\nCollisions using Linear Probing:\t";
	for (auto& list : lists) {
		std::cout << list.second.collisionsLinearProbing << '\t';
	}

	std::cout << "\nCollisions using Quadratic Probing:\t";
	for (auto& list : lists) {
		std::cout << list.second.collisionsQuadraticProbing << '\t';
	}

	std::cout << "\nCollisons using Double Hashing:\t\t";
	for (auto& list : lists) {
		std::cout << list.second.collisionsDoubleHashing << '\t';
	}

	std::cout << "\n#Failures for Quadratic hashing:\t";
	for (auto& list : lists) {
		std::cout << list.second.failuresQuadraticProbing << '\t';
	}

	std::cout << "\n#Failures for Double hashing:\t\t";
	for (auto& list : lists) {
		std::cout << list.second.failuresDoubleHashing << '\t';
	}
}
