// Dusty Franklin
// 316 P4 - Hash resolutions
// COPYRIGHT (C) 2017 Dusty Franklin (df59) All rights reserved.

#include "316p4.hpp"

#include <iostream>

int main() {
	ListsOfInts lists;
start:
	generateRandomLists(lists, 10);

	for (auto& list : lists) {
		try {
			linearProbe(list);
		} catch (std::exception& e) {
			goto start;
		}
		try {
			quadraticProbe(list);
		} catch (std::exception& e) {
			goto start;
		}
		try {
			doubleHash(list);
		} catch (std::exception& e) {
			goto start;
		}
	}

	print(lists);

	return 0;
}
